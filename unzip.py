import zipfile
import os
import pickle
import config
import file_tracker

def unzipUnseen():
	# Check if the pickled 'file_tracker' file exists, if so get it, if not initialisa a dict. 
	if os.path.isfile(config.file_tracker_obj_directory):
		file_tracker = pickle.load(open(config.file_tracker_obj_directory, "r"))
	else:
		file_tracker = {}
		pickle.dump(file_tracker, open(config.file_tracker_obj_directory, "wb"))
	
	# Iterate through all the files in the directory.
	for fname in os.listdir(config.ftp_directory):
		if fname not in file_tracker:
			print fname
			
			# Check if it's a zip file
			if fname.split('.')[-1].lower() == "zip":
				# Unzip the file. 
				zip_ref = zipfile.ZipFile(config.ftp_directory+"/"+fname, 'r')
				zip_ref.extractall(config.unzipped_directory)
				zip_ref.close()

				# Add the file name to 'file_tracker' list and dump the file_tracker  to the file_tracker file for later runs / reference. 
				file_tracker[fname] = ['unzipped']
				pickle.dump(file_tracker, open(config.file_tracker_obj_directory, "wb"))

def unzip(fname):
	# Check if it's a zip file
	if fname.split('.')[-1].lower() == "zip":
		# Unzip the file. 
		zip_ref = zipfile.ZipFile(config.ftp_directory+"/"+fname, 'r')
		zip_ref.extractall(config.unzipped_directory)
		zip_ref.close()

