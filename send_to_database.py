from pymongo import MongoClient
import datetime
import pytz
import config

client = MongoClient(config.mongo_host, config.mongo_port)

def send_one_to_database( dt_obj, increment, id_str, kWh=None, kVArh=None, origin=None):
	# kWh = float(kWh)
	# kVA = float(kVArh)
	increment = float(increment)
	# date_time = int(date_time)
	# dt_obj = dt_obj.replace(tzinfo=pytz.timezone('Australia/Adelaide'))
	if config.mongo_host == 'localhost':
		db = client.meteor
	else:
		db = client.oxamii
		
	data_point =   {
					"meter_id":id_str,
					"date_time" : dt_obj,
					"kWh" : kWh,
					"kVArh" : kVArh,
					"increment": increment,
					"time_added" : datetime.datetime.utcnow(),
					"origin":origin,
					}
	data_points = db.data_points
	# data_id = data_points.insert_one(data_point).inserted_id
	data_points.update({'meter_id':id_str, 'date_time':dt_obj}, data_point, upsert=True)



def read_csv_and_send(filename):
	entries = []
	with open(filename) as csv:
		for line in csv:
			data_entry = entries.append(line.rstrip().split(', '))
	for entry in entries[1:]:
		date_time = entry[0][6:10] + entry[0][3:5] + entry[0][0:2] + entry[1][0:2] + entry[1][3:5] + entry[1][6:8]
		send_to_database(date_time, float(entry[2]), float(entry[3]))




