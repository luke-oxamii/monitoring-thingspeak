path_to_watch = "./ftp"
ftp_directory = "./ftp"
unzipped_directory = "./raw"
file_tracker_obj_directory = "./file_tracker.pkl"
state_tracker_obj_directory = "./state_tracker.pkl"

import os

SECRET_KEY = os.environ.get('IN_A_DOCKER_CONTAINER', False)
SECRET_KEY = True
if SECRET_KEY:
	os.system('echo -e \"\t\" DOCKER CONTAINER DETECTED - USING REMOTE DB CONFIG\\"')
	print "DOCKER CONTAINER DETECTED - USING REMOTE DB CONFIG"
	mongo_host = 'mongodb://monitoring-ftp:Hgits72bfks854@oxamii-shard-00-00-ukep6.mongodb.net:27017,oxamii-shard-00-01-ukep6.mongodb.net:27017,oxamii-shard-00-02-ukep6.mongodb.net:27017/test?ssl=true&replicaSet=oxamii-shard-0&authSource=admin'
	# mongo_host = "mongodb://oxamiidemo:bananas@ds029476.mlab.com:29476/oxamii"
	mongo_port = 27017
else:
	os.system('echo -e \"\t\" NO DOCKER CONTAINER DETECTED - USING LOCAL DB CONFIG\\"')
	print "NO DOCKER CONTAINER DETECTED - USING LOCAL DB CONFIG"
	mongo_host = 'localhost'
	mongo_port = 27017

# mongo_host = 'mongodb://oxamiidemo:bananas@ds029476.mlab.com:29476/oxamii'
# mongo_port = 27017