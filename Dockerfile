FROM python:2.7         
ADD . /monitoring
WORKDIR /monitoring
RUN chmod -R 777 /monitoring
# RUN mkdir -p raw
# RUN mkdir -p ftp
# RUN apt-get update
# RUN apt-get install -y nginx 
# RUN apt-get install -y lftp
# RUN apt-get install -y sudo
EXPOSE 27017 5000
RUN pip --no-cache-dir install -r requirements.txt
ENV IN_A_DOCKER_CONTAINER Yes
ENTRYPOINT ["python", "run.py"]