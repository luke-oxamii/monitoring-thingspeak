from pymongo import MongoClient
import config

client = MongoClient(config.mongo_host, config.mongo_port)

def checkRecorded(filename):
	if config.mongo_host == 'localhost':
		db = client.meteor
	else:
		db = client.oxamii
	
	if db.processed_files.find({'filename':filename}).count() == 0:
		return False
	else:
		return True

def record(filename):
	if config.mongo_host == 'localhost':
		db = client.meteor
	else:
		db = client.oxamii
	
	db.processed_files.insert({'filename':filename, 'messages':[]})

def checkProperty(filename, property_str):
	if config.mongo_host == 'localhost':
		db = client.meteor
	else:
		db = client.oxamii
	
	my_file = db.processed_files.find_one({'filename':filename})
	if not my_file:
		return False
	elif property_str not in my_file:
		return False
	else:
		return True

def recordProperty(filename, property_str):
	if config.mongo_host == 'localhost':
		db = client.meteor
	else:
		db = client.oxamii
	
	my_file = db.processed_files.find_one({'filename':filename})
	if not my_file:
		record(filename)
		my_file = db.processed_files.find_one({'filename':filename})
	my_file[property_str] = True
	db.processed_files.update({'filename':filename}, my_file)


def recordMessage(filename, message_str):
	recordProperty(filename, 'has_message')
	if config.mongo_host == 'localhost':
		db = client.meteor
	else:
		db = client.oxamii
	
	my_file = db.processed_files.find_one({'filename':filename})
	if not my_file:
		record(filename)
		my_file = db.processed_files.find_one({'filename':filename})
	
	my_file['messages'].append(message_str)
	db.processed_files.update({'filename':filename}, my_file)


if __name__ == "__main__":
	names = ["test1", "test2", "test3"]
	for name in names:
		assert not checkRecorded(name)
		record(name)
		assert checkRecorded(name)
		recordMessage(name, "Everything is cool.")
		recordMessage(name, "No need to panic.")
		assert not checkProperty(name, 'barrelled')
		recordProperty(name,'barrelled')
		assert checkProperty(name, 'barrelled')

