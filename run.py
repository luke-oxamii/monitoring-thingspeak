import os, time
import unzip
import config
# import nem12mongo
import pickle
import subprocess
# import file_tracker
import schedule
import thingspeak
import json
from send_to_database import send_one_to_database
import pendulum



field_names=['field1', 'field2', 'field3', 'field4', 'field5', 'field6', 'field7', 'field8']

def update():
	print "Starting update"
	channel = thingspeak.Channel('470040', api_key='6KBBA9WYOJNKTJPA')
	result = channel.get()
	parsed = json.loads(result)
	start_dt = pendulum.parse(parsed['feeds'][0]['created_at'])
	for dp in parsed['feeds']:
		dt_obj = pendulum.parse(dp['created_at'])
		if dt_obj > start_dt.add(minutes=15):
			start_dt = dt_obj
			print dp
			for field in field_names:
				
				send_one_to_database( start_dt, 900, 'aaron-'+field, kWh=float(dp[field])*0.25, kVArh=None, origin=None)


if __name__ == "__main__":	
	print "Starting Monitoring Server - YES"
	update()
	schedule.every(15).minutes.do(update)

	while(True):
		schedule.run_pending()
		time.sleep(600)