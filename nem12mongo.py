import pprint
import datetime as dt
import dateutil.parser
import sys
from send_to_database import send_one_to_database
from pymongo import MongoClient
import config
import os
import pickle
import pytz
import file_tracker

# Utility and databasing functions. 
def getDateTimeFromISO8601String(s):
    d = dateutil.parser.parse(s)
    return d

def addData(dataSets, id, unit, periodLengthMinutes, value, intervalDt):
    # Get the dataset for the given ID.
    if not id in dataSets:
        dataSets[id] = {}
    timeSeries = dataSets[id]

    if not intervalDt.isoformat() in timeSeries:
        timeSeries[intervalDt.isoformat()] = {
            'periodLengthMinutes':int(periodLengthMinutes)
        }
    if int(periodLengthMinutes) != timeSeries[intervalDt.isoformat()]['periodLengthMinutes']:
        raise ValueError('Error: Period Length Mismatch between kVARH and kWh')
    timeSeries[intervalDt.isoformat()][unit] = float(value)


def addDataToDatabase(dataSets, fname):
    print "IDS: "+str(dataSets.keys())
    
    for id_str in dataSets.keys():
        timeSeries = dataSets[id_str]
        for dateString in sorted(timeSeries.keys()):
            # print dateString
            
            date = getDateTimeFromISO8601String(dateString)
            dateOnlyString = date.strftime("%d/%m/%Y")
            timeString = date.strftime("%I:%M:%S %p")
            dateNumberString = date.strftime("%Y%m%d")
            # periodLengthMinutes = timeSeries[dateString]['periodLengthMinutes']

            kWh = None if not "KWH" in timeSeries[dateString] else timeSeries[dateString]['KWH']
            if kWh == None and "WH" in timeSeries[dateString]:
                kWh = str(float(timeSeries[dateString]['WH']) / 1000.0)

            kVARH = None if not "KVARH" in timeSeries[dateString] else timeSeries[dateString]['KVARH']
            if kVARH == None and "VARH" in timeSeries[dateString]:
                kVARH = str(float(timeSeries[dateString]['VARH'])/1000.0)

            send_one_to_database( date, float(timeSeries[dateString]['periodLengthMinutes'])* 60.0, id_str,  kWh=kWh, kVArh = kVARH, origin=fname)



def scrapeNEM12(directory, filename):
    fname = directory + "/" + filename
    dataSets = {}
    f = open(fname, "rU")
    lines = f.readlines()
    data = {}
    data['records'] = []

    FLAG400 = False
    FLAG500 = False

    # Main NEM12 scraping program logic.
    for line in lines:
        # print line
        line = line.rstrip() #get rid of trailing newline characters
        line = line.split(',')
        # print str(line)
        
        if line[0] == '100':
            
            data['versionHeader'] = line[1]
            data['fileCreatedStr'] = line[2]
            data['fromParticipant'] = line[3]
            data['toParticipant'] = line[4]
        elif line[0] == '200':
            
            data['records'].append({
                'nmi':line[1],
                'nmiConfiguration':line[2],
                'registerID':line[3],
                'nmiSuffix':line[4],
                'mdmDataStreamID':line[5],
                'meterSerialNumber':line[6],
                'uom':line[7],
                'intervalLength':int(line[8]),
                'nextScheduleReadDateString':line[9],
                'days':[], #this contains the '300' data
                'manualReads':[] , #this contains the '400' data
            })
        elif line[0] == '300':
            currentRecord = data['records'][-1] #current record is the latest in the records list. 
            intervalDt = dt.datetime(int(line[1][0:4]),int(line[1][4:6]), int(line[1][6:8]),0,0, tzinfo=pytz.timezone('Australia/Adelaide'))
            numRecords = 1440 / currentRecord['intervalLength']
            dataLine = {
                'recordIndicator':line[0],
                'intervalDateString':line[1],
                'intervalDt':str(intervalDt),
                'dataPoints':line[2: 2 + numRecords], #get the records - third element until the number of records. 
                'qualityMethod':line[-5],
                'reasonCode':line[-4],
                'reasonDescription':line[-3],
                'updateDateTimeStr':line[-2],
                'msatsLoadDateTimeStr':line[-1],
                'events':[]#this contains the '400' event data if there is any.
            }
            currentRecord['days'].append(dataLine) #add the day's interval data to the current record's list of days. 

            for dataPoint in dataLine['dataPoints']:
                intervalDt = intervalDt + dt.timedelta(0,int(currentRecord['intervalLength'])*60) #First record is 00:15 so adding 15 here is fine. This makes the records time-ending.
                # id_string = currentRecord['nmi']+"-"+currentRecord['nmiSuffix']+"-"+currentRecord['registerID']+"-"+currentRecord['mdmDataStreamID']
                # I took out the registerID below because the MDP only included it after December 1 2017. 
                # This needs a refactor so that all dps at a certain time from the same NMI are recorded under the same document in the db
                id_string = currentRecord['nmi']+"-"+currentRecord['nmiSuffix']+"-"+currentRecord['mdmDataStreamID']
                addData(dataSets, id_string,currentRecord['uom'], currentRecord['intervalLength'], float(dataPoint), intervalDt)

        elif line[0] == '400':
            
            FLAG400 = True
            currentRecord = data['records'][-1] #current record is the latest in the records list. 
            # find the corresponding day
            currentDay = currentRecord['days'][-1]
            currentDay['events'].append({
                'recordIndicator':line[0],
                'startIntervalStr':line[1],
                'endIntervalStr':line[2],
                'qualityMethod':line[3],
                'reasonCode':line[4],
                'reasonDescription':line[5]
            })
        
        elif line[0] == '500':
            
            FLAG500 = True
            currentRecord = data['records'][-1] #current record is the latest in the records list. 
            currentRecord['manualReads'].append({
                'recordIndicator':line[0],
                'transCode':line[1],
                'retServiceOrder':line[2],
                'readDateTime':line[3],
                'indexRead':line[4]
            })
        elif line[0] =='900':
            
            pp = pprint.PrettyPrinter(indent=4)
            # pp.pprint(data)
        else:
            raise ValueError('Line code not understood: '+str(line[0]))

    addDataToDatabase(dataSets, filename)

    if FLAG400:
        file_tracker.recordMessage(filename, "WARNING: NEM CODE 400 DETECTED, SUGGEST MANUAL EXAMINATION - SEE NEM12 SPEC")
    if FLAG500:
        file_tracker.recordMessage(filename, "WARNING: NEM CODE 500 DETECTED, SUGGEST MANUAL EXAMINATION - SEE NEM12 SPEC")

def scrapeAllUnscraped():
    # Check if the pickled 'file_tracker' file exists, if so get it, if not initialisa a dict. 
    if os.path.isfile(config.file_tracker_obj_directory):
        file_tracker = pickle.load(open(config.file_tracker_obj_directory, "r"))
    else:
        file_tracker = {}
        pickle.dump(file_tracker, open(config.file_tracker_obj_directory, "wb"))
    for fname in os.listdir(config.unzipped_directory):
        # if we haven't seen this file before, add it to the list. 
        if fname not in file_tracker:
            file_tracker[fname] = []
        # if the File is a csv and has not been scraped, scrape it.
        if fname.split('.')[-1].lower() == "csv" and 'scraped' not in file_tracker[fname]:
            print "Scraping and adding to database: "+fname
            scrapeNEM12(config.unzipped_directory, fname)
            file_tracker[fname] = ['scraped']
            pickle.dump(file_tracker, open(config.file_tracker_obj_directory, "wb"))
            print "Finished Scraping: "+fname

def scrape(fname):
    # if the File is a csv and has not been scraped, scrape it.
    if fname.split('.')[-1].lower() == "csv":
        print "Scraping and adding to database: "+fname
        scrapeNEM12(config.unzipped_directory,fname)
        print "Finished Scraping: "+fname
