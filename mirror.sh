#!/bin/bash
echo ">>> $(date): Starting Mirroring Request"
REMOTE_HOST=54.153.250.105
LOCAL_DIR=${PWD}/ftp
REMOTE_DIR=/
lftp -u selectsolutions,ospift90s2c -e "mirror  --parallel=3 --verbose /$REMOTE_DIR /$LOCAL_DIR  && exit" $REMOTE_HOST
echo ">>> $(date): Mirroring Complete"
